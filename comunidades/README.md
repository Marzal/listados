# Comunidades con apoyo explícito a las tecnologías y cultura libres

En <https://listados.eslib.re/comunidades/> puedes encontrar listadas las páginas con información de las siguientes comunidades que tienen entre sus intenciones mostrar un apoyo explícito a las tecnologías y cultura libres a través de sus actividades:

-   [AGASOL - Asociacion Galega de Software Libre](agasol.md)
-   [ASOLIF - Federación Nacional de Empresas de Software Libre](asolif.md)
-   [Elbinario](elbinario.md)
-   [GALPon - Grupo de Amigos de Linux de Pontevedra](galpon.md)
-   [GNOME Hispano](gnome-hispano.md)
-   [GNU/Linux Valencia](gnulinux-vlc.md)
-   [GPUL - Grupo de Programadores e Usuarios de Linux](gpul.md)
-   [GULIC - Grupo de Usuarios de Linux de Canarias](gulic.md)
-   [Interferencias](interferencias.md)
-   [KDE España](kde-es.md)
-   [LibreIM](libreim.md)
-   [LibreLabGRX](librelabgrx.md)
-   [Wikiesfera - Grupo de usuarixs](wikiesfera.md)
-   [Wikimedia España](wikimedia-es.md)

Si conoces alguna que no aparece aquí puedes añadirla directamente en este mismo archivo, pero si puedes, te agradeceríamos que añadieras en esta misma carpeta su página de información usando [esta plantilla](../plantillas/comunidades.md).
