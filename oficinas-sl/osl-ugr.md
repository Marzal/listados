---
layout: post
section: offices
title: Oficina de Software Libre de la Universidad de Granada
---

## Información

Operativa desde el año 2008, la Oficina de Software Libre ha dedicado su actividad a la promoción y el fomento del software libre dentro y fuera de la UGR, centrando sus recursos y personal en la divulgación así como en promulgar sus numerosos beneficios. Con sede ubicada en Granada, la oficina aplica su actividad en toda la provincia.

-   Director/a: Pablo García Sánchez
-   Email: <mailto:dirosl@ugr.es>

## Contacto

-   Dirección física: C/ Real de Cartuja 36-38 (Edificio CEPRUD) 18012 Granada
-   Página web: <https://osl.ugr.es/>
-   Email: <mailto:osl@ugr.es>
-   Teléfono: 958 241000 (extensión 20207)
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/OSLUGR>
-   Sala en Matrix:
-   Grupo o canal en Telegram: <https://t.me/oslugr>
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://github.com/oslugr/>
