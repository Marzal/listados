# Oficinas de Software Libre en las Universidades

En <https://listados.eslib.re/oficinas/> puedes encontrar listadas las páginas con información de las siguientes oficinas de software libre pertenencientes a alguna universidad:

-   [Oficina de Conocimiento y Cultura Libres de la Universidad Rey Juan Carlos](ofilibre-urjc.md)
-   [Oficina de Software Libre de la Universidad de Granada](osl-ugr.md)

Si conoces alguna que no aparece aquí puedes añadirla directamente en este mismo archivo, pero si puedes, te agradeceríamos que añadieras en esta misma carpeta su página de información usando [esta plantilla](../plantillas/oficinas.md).
