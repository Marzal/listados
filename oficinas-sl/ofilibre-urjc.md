---
layout: post
section: offices
title: Oficina de Conocimiento y Cultura Libres de la Universidad Rey Juan Carlos
---

## Información

La Oficina de Conocimiento y Cultura Libres (OfiLibre) quiere ayudar a la comunidad universitaria a comprender la cultura libre, la publicación libre, los datos abiertos y el software libre. Dedicándose para ello a explicar qué son, cómo funcionan, qué características tienen, y qué beneficios pueden producir, de forma que cualquiera pueda decidir cómo le conviene relacionarse con ellos.

-   Director/a: Jesús M. González Barahona
-   Email: <mailto:jesus.gonzalez.barahona@urjc.es>

## Contacto

-   Dirección física: C/ Tulipán s/n (Campus de Móstoles. Despacho 004, planta baja edificio Rectorado) 28933 Móstoles (Madrid)
-   Página web: <https://ofilibre.gitlab.io/>
-   Email: <mailto:ofilibre@urjc.es>
-   Teléfono: 91 4884679
-   Mastodon (u otras redes sociales libres): <https://floss.social/@OfiLibreURJC>
-   Twitter: <https://twitter.com/OfiLibreURJC>
-   Sala en Matrix:
-   Grupo o canal en Telegram: <https://t.me/ofilibreurjc>
-   GitLab/GitHub (u otros sitios de código colaborativo): <https://gitlab.com/ofilibre/ofilibre.gitlab.io>
