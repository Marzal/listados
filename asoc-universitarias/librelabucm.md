---
layout: post
section: associations
title: LibreLabUCM
---

## Información

LibreLabUCM (LLU) es una asociación de apasionados por el software libre y la ciberseguridad.. Nuestra asociación nace con la vocación de usar y promover el software libre, y, al mismo tiempo, aprender con él tanto dentro como fuera de la universidad.

-   Universidad a la que está asociada: Universidad Complutense de Madrid

## Contacto

-   Sede física:
-   Página web: <https://librelabucm.org/>
-   Email: <mailto:librelab@ucm.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.librelabucm.org/@librelabucm>
-   Twitter: <https://twitter.com/LibreLabUCM>
-   Sala en Matrix:
-   Grupo o canal en Telegram: <https://t.me/LibreLab>
-   GitLab/GitHub (u otros sitios de código colaborativo):
