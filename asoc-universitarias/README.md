# Asociaciones Estudiantiles Universitarias Libres

En <https://listados.eslib.re/asociaciones/> puedes encontrar listadas las páginas con información de las siguientes asociaciones estudiantes universitarias que tienen como objetivo la divulgación de tecnologías y cultura libres a través de sus actividades:

-   [LibreLabUCM](librelabucm.md)

Si conoces alguna que no aparece aquí puedes añadirla directamente en este mismo archivo, pero si puedes, te agradeceríamos que añadieras en esta misma carpeta su página de información usando [esta plantilla](../plantillas/asociaciones.md).
